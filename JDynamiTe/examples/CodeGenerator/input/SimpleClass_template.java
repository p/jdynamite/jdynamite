/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * {CLASS_NAME}: generated by JDynamiTe from SimpleClass_template.java
 */

import java.util.*;

public class {CLASS_NAME} {
   <!-- BEGIN DYNAMIC : private_members -->
	private {MEMBER_TYPE} {MEMBER_NAME};
   <!-- END DYNAMIC : private_members -->
      
   // no method declaration in Java (you could remove methods_decl block declaration from the template)
   <!-- BEGIN DYNAMIC : methods_decl -->
   <!-- END DYNAMIC : methods_decl -->

   <!-- BEGIN DYNAMIC : methods_def -->
   public void set_{MEMBER_NAME}({MEMBER_TYPE} value) {
      {MEMBER_NAME} = value;
   }
   public {MEMBER_TYPE} get_{MEMBER_NAME}() {
     return {MEMBER_NAME};
   }

   <!-- END DYNAMIC : methods_def -->
};
