<%@ page import="cb.jdynamite.JDynamiTe, cb.jdynamite.examples.JDynamiTeQuickStart" %>
<jsp:useBean id="jDynQuickStart" scope="request" class="cb.jdynamite.examples.JDynamiTeQuickStart"/>

<%
	JDynamiTe jDyn;
	String inputTemplateName;
	String xmlInputFileName;
	String exampleRootPath;
	String title;
	boolean verbose;
	
	inputTemplateName = application.getRealPath("QuickStart/input/testTemplateV2_0.html");
	xmlInputFileName = application.getRealPath("QuickStart/input/my_xml_input.xml");
	exampleRootPath = "."; // must correspond to "QuickStart" directory. In this case, this is the current working dir.
	title = "\"JDynamiTeQuickStart\" - JSP, classic syntax";
	verbose = false;
	
	jDyn = jDynQuickStart.run(inputTemplateName, xmlInputFileName, exampleRootPath, title, verbose);
	
	if (jDyn != null)
		out.println(jDyn.toString()); // send HTML page
	else
		out.println("Error: can not run JSP (classic syntax) JDynamiTeQuickStart example !!!");
%>
