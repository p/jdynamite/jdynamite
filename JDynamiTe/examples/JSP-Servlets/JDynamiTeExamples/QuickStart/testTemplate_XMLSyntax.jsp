<?xml version="1.0" encoding="ISO-8859-1" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0">
	<jsp:directive.page contentType="text/html; charset=ISO-8859-1" 
		pageEncoding="ISO-8859-1" session="false"/>
	<jsp:output doctype-root-element="html"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="true" />

	<jsp:directive.page import="cb.jdynamite.JDynamiTe, cb.jdynamite.examples.JDynamiTeQuickStart" />
    <jsp:useBean id="jDynQuickStart" scope="request" class="cb.jdynamite.examples.JDynamiTeQuickStart"/>

	<jsp:scriptlet>
	
	JDynamiTe jDyn;
	String inputTemplateName;
	String xmlInputFileName;
	String exampleRootPath;
	String title;
	boolean verbose;
	
	inputTemplateName = application.getRealPath("QuickStart/input/testTemplateV2_0.html");
	xmlInputFileName = application.getRealPath("QuickStart/input/my_xml_input.xml");
	exampleRootPath = "."; // must correspond to "QuickStart" directory. In this case, this is the current working dir.
	title = "\"JDynamiTeQuickStart\" - JSP, XML syntax";
	verbose = false;
	
	jDyn = jDynQuickStart.run(inputTemplateName, xmlInputFileName, exampleRootPath, title, verbose);
	
	if (jDyn != null)
	    out.println(jDyn.toString()); // send HTML page
	else
	    out.println("Error: can not run JSP (XML syntax) JDynamiTeQuickStart example !!!");

	</jsp:scriptlet>

</jsp:root>