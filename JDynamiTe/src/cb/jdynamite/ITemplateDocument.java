/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite;

import cb.jdynamite.analyser.*;


public interface ITemplateDocument {
    public String getVariable(String key);
    public void setVariable(String key, String value);
    void recordDynElem(String key, IDynamicElement value);
    public ITemplateAnalyser getAnalyser();
    public void setAnalyser(ITemplateAnalyser templateAnalyser);
    public void setLastError(String lastError);
    public String getLastError();
}
