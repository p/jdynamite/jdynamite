/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.examples;

import java.io.File;
import java.util.*;
import cb.jdynamite.JDynamiTe;


/**
 * Provides a simple (but sufficient) example of JDynamiTe use.
 * Needs "testTemplateV2_0.html" as template file.
 * Writes the result on standard output.
 */
public class JDynamiTeQuickStart {

    public JDynamiTe run(
        String inputTemplateFileName,
        String xmlInputFileName,
        String exampleRootPath,
        String title,
        boolean verbose) {

        JDynamiTe dynamiTe = new JDynamiTe("QuickStartTest");

        // Set verbose mode if you want additional messages (on stderr output)
        JDynamiTe.setVerbose(true);
        
        // 1) Use "setInput" method to define (and analyse) the input template file.
        try {
            dynamiTe.setInput(inputTemplateFileName);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
        
        // possibly see the template document structure (if verbose/debug)
        if (verbose) {
            String totalDef = dynamiTe.getDefinition(0);
            System.err.println("\n----- definition -----\n\n" + totalDef + "\n-----\n");
            
            System.err.println("\n===== keys =====\n");
            Enumeration<String> keys = dynamiTe.getVariableKeys();
            while (keys.hasMoreElements()) {
                System.err.println("[" + keys.nextElement() + "]");
            }
            System.err.println("\n=====\n");
        }
        //
        
        // 2) Use "setVariable" method to give a value to variables.
        dynamiTe.setVariable("THE_TITLE", title);
        dynamiTe.setVariable("__INPUT_TEMPLATE__", exampleRootPath + "/input/" + new File(inputTemplateFileName).getName());
        
        // First table
        for (int i = 0; i < 6; i++) {
            dynamiTe.setVariable("COL1", "line_" + i + ",col_1");
            dynamiTe.setVariable("COL2", "line_" + i + ",col_2");
            // 3) Use "parseDynElem" to build a Dynamic Element
            dynamiTe.parseDynElem("mySimpleRow"); // add a row
            // "parseDynElem" appends the Dynamic Element definition to its current value
        }
        
        // A message
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("EEEEEE hh'h'mm'm'ss's'");
        String dateString = formatter.format(new java.util.Date());
        dynamiTe.setVariable("A_MESSAGE", "Hello Duke, " + dateString);
        
        // An image list
        String imagePath = exampleRootPath + "/input/images/duke";
        for (int px = 0; px < 3; px++) {
            dynamiTe.setVariable("PICTURE", imagePath + px + ".gif");
            dynamiTe.parseDynElem("somePictures");
        }
        
        // Second table with nested Dynamic Element
        for (int row = 0; row < 5; row++) {
            // first group of columns
            // 4) Use "setDynElemValue" to set or reset the value of a Dynamic Element
            dynamiTe.setDynElemValue("colX", ""); // reset for each row
            for (int col = 0; col < 3; col++) {
                dynamiTe.setVariable("VALUE_X", "line_" + row + ",col_" + col);
                dynamiTe.parseDynElem("colX"); // add a column
            }
            // second group of columns
            dynamiTe.setDynElemValue("colY", ""); // reset for each row
            for (int col = 3; col < 5; col++) {
                dynamiTe.setVariable("VALUE_Y", "line_" + row + ",col(BIS)_" + col);
                dynamiTe.parseDynElem("colY"); // add a column
            }
            dynamiTe.parseDynElem("myBigRow"); // add a row
        }
        
        // XML block example
        /* Input data come from an XML file referenced into the input template file,
           (my_xml_input.xml by default). Nothing else to write ... */
        if (xmlInputFileName != null)
            dynamiTe.setXMLInput("myXML_block", xmlInputFileName);

        /* parseDynElem: Second argument set to true allows to recursively parse an XML block */
        dynamiTe.parseXMLDynElem("myXML_block", true); // that's all !
        // 5) Check if an error occurred.
        /* Errors can typically happen when an external XML input document can not be open.
         * Note that the special variable tag "{__LAST_JDYN_ERROR__}" is automatically populated by JDynamiTe, after parsing Dynamic Elements.
         * So if this tag is included in your template, it will automatically contain the error message (if any, else it is empty).
         */
        String error = dynamiTe.getLastError(); 
        if (error != null)
            System.err.println("JDynamiTe parsing error: " + error);

        // 6) Use "parse" to finally get the value of your Dynamic Template Document
        dynamiTe.parse();
        return dynamiTe;
    }

    public static void main(String args[]) {
        String inputTemplateName;
        if (args.length > 0)
            inputTemplateName = args[0];
        else
            inputTemplateName = "examples/QuickStart/input/testTemplateV2_0.html";

        JDynamiTe jDyn = new JDynamiTeQuickStart().run(inputTemplateName,
                null, "..", "\"JDynamiTe - Java Dynamic Template\" test, standalone appli mode", true);
        if (jDyn != null)
            System.out.println(jDyn.toString());
    }
}

