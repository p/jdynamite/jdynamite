/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.examples;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cb.jdynamite.JDynamiTe;
import cb.jdynamite.tool.JDynTool;

public class RSSReaderServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String INPUT_TEMPLATE = "RSSReader/input/templ_RSS_Feed_param.html";
	private static final String INPUT_URI_PROPOSAL = "RSSReader/input/RSS_feed_URI_list.xml";
	private static final String CSS = "jDyn_example.css";
	private static final String CSS_PATH_VAR_TAG = "__CSS__";
	private static final String FEED_URI_CHOICE = "feedURIChoice";
	private static final String FEED_URI_PROPOSAL = "feedURIProposal";
	private static final String FEED_URI_CHOICE_TAG = "FEED_URI_CHOICE";
	private static final String INPUT_URI_PROPOSAL_PATH_TAG = "INPUT_URI_PROPOSAL_PATH";
	private static final String DEFAULT_FEED_URI = "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml";
	private static final String CLEAR_CACHE_CHECK = "clearCache";

	private String selectedURI;
	private String xmlInputProposalPath;
	private Hashtable<String, String> keyValuesList;
	private boolean verbose = true;
	private JDynTool jDynTool;
	private JDynamiTe jDyn;
	private String clearCacheCheck;
	
	public RSSReaderServlet() {
		super();
		if (verbose)
			System.out.println("RSSReaderServlet.RSSReaderServlet()");
		jDynTool = new JDynTool();
		keyValuesList = new Hashtable<String, String>();
		keyValuesList.put(RSSReaderServlet.CSS_PATH_VAR_TAG, RSSReaderServlet.CSS);
		keyValuesList.put(JDynTool.INPUT_TEMPLATE_PATH_VAR_TAG, RSSReaderServlet.INPUT_TEMPLATE);
		keyValuesList.put("THE_TITLE", "JDynamiTe RSSReaderServlet Example");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if (verbose)
			System.out.println("RSSReaderServlet.doGet()");
		selectedURI = request.getParameter(RSSReaderServlet.FEED_URI_PROPOSAL);
		if (selectedURI == null)
			selectedURI = request.getParameter(RSSReaderServlet.FEED_URI_CHOICE);
		if (selectedURI == null) {
			selectedURI = RSSReaderServlet.DEFAULT_FEED_URI;
		}
		keyValuesList.put(RSSReaderServlet.FEED_URI_CHOICE_TAG, selectedURI);
		
		if (xmlInputProposalPath == null) {
			xmlInputProposalPath = request.getSession().getServletContext().getRealPath(RSSReaderServlet.INPUT_URI_PROPOSAL);
			keyValuesList.put(RSSReaderServlet.INPUT_URI_PROPOSAL_PATH_TAG, xmlInputProposalPath);
		}
		if (jDyn == null) {
			String inputTemplateFile = request.getSession().getServletContext().getRealPath(RSSReaderServlet.INPUT_TEMPLATE);
			jDyn = jDynTool.run(inputTemplateFile, null, keyValuesList, verbose);
		}
		else {
			clearCacheCheck = request.getParameter(RSSReaderServlet.CLEAR_CACHE_CHECK);
			System.err.println("clearCacheCheck=" + clearCacheCheck);
			if (clearCacheCheck != null) {
				jDyn.resetExternalXMLDocuments(); // clear cache XML doc. list
			}
			jDyn = jDynTool.run(null, keyValuesList, verbose);
		}
		PrintWriter out = response.getWriter();
		if (jDyn != null) {
			// Check if an error occurred.
			/* Note that the special variable tag "{__LAST_JDYN_ERROR__}" is automatically populated by JDynamiTe, after parsing Dynamic Elements.
			 * So if this tag is included in your template, it will automatically contain the error message (if any, else it is empty).
			 * Errors can typically happen when an external XML input document can not be open.
			 */
			String error = jDyn.getLastError(); 
			if (error != null)
				System.err.println("Error JDyn: " + error);
			// Send final document
			out.println(jDyn.toString());
		}
		else {
			System.err.println("Error: can not run JDynamiTe RSSReaderServlet example: invalid input template file !!!");
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "JDynamiTe RSSReaderServlet error: can not find template: " + RSSReaderServlet.INPUT_TEMPLATE);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if (verbose)
			System.out.println("RSSReaderServlet.doPost()");
		doGet(request, response);
	}
}
