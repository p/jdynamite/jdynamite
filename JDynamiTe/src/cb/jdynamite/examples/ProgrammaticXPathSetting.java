/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.examples;

import java.util.*;
import cb.jdynamite.JDynamiTe;


/**
 * Provides a complete (!!!) example of JDynamiTe XML input feature.
 * Needs "Football_templ_2.txt" as template file.
 * Writes the result on standard output.
 */
public class ProgrammaticXPathSetting {

    public void run(String inputTemplateFileName) {
        JDynamiTe dynamiTe = new JDynamiTe("XMLVariableExpressionExample");

        // Set verbose mode if you want additional messages (on stderr output)
        JDynamiTe.setVerbose(true);
        
        // 1) Use "setInput" method to define (and analyse) the input template file.
        try {
            dynamiTe.setInput(inputTemplateFileName);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
        // possibly see the template document structure (for debug)
        String totalDef = dynamiTe.getDefinition(0);
        System.err.println("\n----- definition -----\n\n" + totalDef + "\n-----\n");
        
        System.err.println("\n===== keys =====\n");
        Enumeration<String> keys = dynamiTe.getVariableKeys();
        while (keys.hasMoreElements()) {
            System.err.println("[" + keys.nextElement() + "]");
        }
        System.err.println("\n=====\n");
        //
        
        // 2) Use "setVariable" method to give a value to variables.
        dynamiTe.setVariable("THE_TITLE", "Football_World");
      
        // XML block "Countries" : set the XML input file
        dynamiTe.setXMLInput("Countries", "examples/XML_variable_expression/input/Football_Clubs.xml");
        
        // XML block "Clubs" : set the XML XPath Expression to select all club items,
        //                     relatively to the parent block ("Countries")
        dynamiTe.setXMLXPathExpression("Clubs", "./club");

        // XML block "Players" : set the XML input file
        dynamiTe.setXMLInput("Players", "examples/XML_variable_expression/input/Football_Players.xml");

        for (int i=1; dynamiTe.hasXMLElement("Countries"); i++) {
            dynamiTe.setVariable("COUNTRY_NB", String.valueOf(i));
            dynamiTe.setDynElemValue("Clubs", "");
            for (int j=1; dynamiTe.hasXMLElement("Clubs"); j++) {
                dynamiTe.setVariable("CLUB_NB", String.valueOf(j));
                String currentClubName = dynamiTe.getXMLVariable("Clubs", "@title");
                String playersXPathExpr = "//club[@name='" + currentClubName + "']/famousPlayer";
                dynamiTe.setXMLXPathExpression("Players", playersXPathExpr);
                dynamiTe.setDynElemValue("Players", "");
                for (int k=1; dynamiTe.hasXMLElement("Players"); k++) {
                    dynamiTe.setVariable("PLAYER_NB", String.valueOf(k));
                    dynamiTe.parseDynElem("Players");
                }
                dynamiTe.parseDynElem("Clubs");
            }
            dynamiTe.parseDynElem("Countries");
        }
                
        // 5) Use "parse" to finaly get the value of your Dynamic Template Document
        dynamiTe.parse();
        System.out.println(dynamiTe.toString());
    }

    public static void main(String args[]) {
        String inputTemplateName;
        if (args.length > 0)
            inputTemplateName = args[0];
        else
            inputTemplateName = "examples/XML_variable_expression/input/Football_templ_2.txt";

        new ProgrammaticXPathSetting().run(inputTemplateName);
    }

}

