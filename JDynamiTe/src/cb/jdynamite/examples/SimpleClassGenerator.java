/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.examples;

import cb.jdynamite.JDynamiTe;


/**
 * Provides a simple (but sufficient) example of JDynamiTe use.
 * Needs "SimpleClass_template.cpp" or "SimpleClass_template.java" as template file.
 * Writes the result on standard output.
 */
public class SimpleClassGenerator {

    private String memberName[] = {"title", 	"height", 	"width"};
    private String memberType[] = {"string", 	"int", 		"int"};

    private String dynamicBlocks[] = {"private_members", "methods_decl", "methods_def"};

    public void run(String inputTemplateFileName) {
        JDynamiTe dynamiTe = new JDynamiTe("SimpleClassGenerator");

        // Set verbose mode if you want additional messages (on stderr output)
        JDynamiTe.setVerbose(true);
        
        // 1) Use "setInput" method to define (and analyse) the input template file.
        try {
            dynamiTe.setInput(inputTemplateFileName);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
               
        // 2) Use "setVariable" method to give a value to variables.
        dynamiTe.setVariable("CLASS_NAME", "MySimpleClass");
        
        for (int blockNb = 0; blockNb < dynamicBlocks.length; blockNb++) {
            for (int i = 0; i < memberName.length; i++) {
                dynamiTe.setVariable("MEMBER_TYPE", memberType[i]);
                dynamiTe.setVariable("MEMBER_NAME", memberName[i]);
                // 3) Use "parseDynElem" to build a Dynamic Element
                dynamiTe.parseDynElem(dynamicBlocks[blockNb]);  // expand this current block occurrence
                // "parseDynElem" appends the Dynamic Element definition to its current value
            }        	
        }
        
        // 5) Use "parse" to finally get the value of your Dynamic Template Document
        dynamiTe.parse();
        System.out.println(dynamiTe.toString());
    }

    public static void main(String args[]) {
        String inputTemplateName;
        if (args.length > 0)
            inputTemplateName = args[0];
        else
            inputTemplateName = "examples/CodeGenerator/input/SimpleClass_template.cpp";

        new SimpleClassGenerator().run(inputTemplateName);
    }

}

