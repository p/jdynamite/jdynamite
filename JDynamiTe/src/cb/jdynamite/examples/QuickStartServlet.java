/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.examples;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import cb.jdynamite.JDynamiTe;
import cb.jdynamite.examples.JDynamiTeQuickStart;


public class QuickStartServlet extends HttpServlet {
	private JDynamiTe jDyn;
	private String inputTemplateName;
	private String xmlInputFileName;
	private String exampleRootPath;
	private static final long serialVersionUID = 1620932792141194831L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
		PrintWriter out = response.getWriter();
		inputTemplateName = request.getSession().getServletContext().getRealPath("QuickStart/input/testTemplateV2_0.html");
		xmlInputFileName = request.getSession().getServletContext().getRealPath("QuickStart/input/my_xml_input.xml");
		exampleRootPath = "QuickStart";

		jDyn = new JDynamiTeQuickStart().run(
					inputTemplateName,
					xmlInputFileName,
					exampleRootPath,
					"\"JDynamiTeQuickStart\" - Servlet mode",
					false);			
		if (jDyn != null)
			out.println(jDyn.toString());
		else
			out.println("Error: can not run JDynamiTeQuickStart example !!!");
    }
	
	// TODO: implement doPost ???
}


