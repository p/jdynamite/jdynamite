/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.analyser;

import cb.jdynamite.ITemplateDocument;


public class XMLVariableElement implements ITemplateElement {
    private String key;
    private IDynamicElement fatherXMLDynElement;
    
    public XMLVariableElement(String key, IDynamicElement fatherXMLDynElement) {
        this.key = key;
        this.fatherXMLDynElement = fatherXMLDynElement;
    }
    
    public String getValue(ITemplateDocument rootDocument) {
    	String value = fatherXMLDynElement.getXMLValue(key);
    	return value;
    }
    
    public String getDefinition(int depth) {
        StringBuffer def = new StringBuffer();
        for (int indent = 0; indent < depth; indent++) {
            def.append("   ");
        }
        def.append("XMLVariableElement: ");
        def.append(key);
        def.append('\n');
        return def.toString();
    }
}
