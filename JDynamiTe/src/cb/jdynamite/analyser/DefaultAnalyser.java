/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.analyser;

import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import cb.jdynamite.*;

/**
 * This class analyses the input template document and builds the JDynamiTe document structure,
 * which is made up of "ITemplateElements".<br>
 * A JDynamiTe document is structurally a tree where the nodes are "DynamicElement" objects,
 * and where the leaves are the other objects.<br>
 * An analyser is attached to an "ITemplateDocument" (JDynamiTe object),
 * which is the "root" Dynamic Element of the tree.
 * 
 * @see cb.jdynamite.JDynamiTe
 */
public class DefaultAnalyser implements ITemplateAnalyser {
    private ITemplateDocument rootDoc;
    private BufferedReader input;
    private Pattern varRegExpPattern;
    private Pattern dynBeginRegExpPattern;
    private Pattern dynEndRegExpPattern;
    private Pattern ignBeginRegExpPattern;
    private Pattern ignEndRegExpPattern;
    // XML
    private Pattern dynBeginXMLRegExpPattern;
    private Pattern xmlVarRegExpPattern;

    private boolean debug = false;
    
    /**
      HTML default "Variable" tag.
      This is the regular expression for a word (within brackets) made up of alphanumeric (and '.', '_', '-') character(s).
      Examples : "Table2.name" or "page-title".<br>
      "[\w\._\-]+" is the regular expression for a word made up of alphanumeric (and '.', '_', '-') character(s).
      <br>Important: the variable name (the key) is the word which matches the first "regexp" subexpression.
      That's why this word should be enclosed within brackets.
      @see java.util.regex
    */
    public static String HTML_VARIABLE_TAG = "\\{([\\w\\._\\-]+)\\}";

    public static String BLOCK_NAME_REGEX = "([\\w\\._\\-]+)";

    /**
      HTML default "Begin Dynamic Element" tag.
      This regular expression is : <pre>
      "&lt;!-- *BEGIN *DYNAMIC.*: *([\w\._\-]+) *--&gt;".
      </pre>For example:<pre>
      "&lt;-- BEGIN DYNAMIC : myList --&gt;".
      </pre>or<pre>
      "&lt;-- BEGIN DYNAMIC BLOCK: myList --&gt;".
      </pre>Important: the element name is this word which matches the first "regexp" subexpression.
      That's why this word should be enclosed within brackets.
      @see java.util.regex
    */
    public static String HTML_BEGIN_DYNAMIC_TAG = "\\s*<!-- *BEGIN *DYN(?:AMIC)? *: *" + BLOCK_NAME_REGEX + " *-->";

    // URI : combination of alphanumerics as well as following characters:  . _ : ? @ / * ] [ = # $ & - / < > { }
    public static String URI_REGEX = "([\\w\\._:\\?@/\\*\\]\\[=#\\$&\\-/<>\\{\\}]+)";
    // XPATH_EXPR : combination of alphanumerics as well as following characters:  . _ - @ / * ( ) ] [ = < > { } '
    public static String XPATH_EXPR_REGEX = "([\\w\\._\\-@/\\*\\(\\)\\]\\[=<>\\{\\}']+)";
    public static String XMLINPUT_VARIABLE_BEGIN = "\\{\\{(";
    public static String XMLINPUT_VARIABLE_END = ")\\}\\}";
    
    public static String HTML_XMLINPUT_VARIABLE_TAG = XMLINPUT_VARIABLE_BEGIN + XPATH_EXPR_REGEX + XMLINPUT_VARIABLE_END;

    //  HTML_BEGIN_XMLINPUT_DYNAMIC_TAG = "<!-- *BEGIN *XML *DYNAMIC : block_name [URI|filename] XPathExpression -->"
    public static String HTML_BEGIN_XMLINPUT_DYNAMIC_TAG = "\\s*<!-- *BEGIN *XML *DYN(?:AMIC)? *: *"
												    	   + BLOCK_NAME_REGEX + "\\s*"
												    	   + URI_REGEX + '?' + "\\s+"
												    	   + XPATH_EXPR_REGEX + " *-->";
    
        
    /**
      HTML default "End Dynamic Element" tag.
      This regular expression is : <pre>
      "&lt;-- *END *DYNAMIC.*--&gt;".
      </pre>For example:<pre>
      "&lt;-- END DYNAMIC : myList --&gt;".
      </pre>or<pre>
      "&lt;-- END DYNAMIC BLOCK: myList --&gt;".
      @see java.util.regex
    */
    public static String HTML_END_DYNAMIC_TAG = "\\s*<!-- *END *DYN(?:AMIC)?.*-->";
    /**
     * HTML default "Begin Ignored Element" tag.
     * This regular expression is : <pre>
     * "&lt;!-- *BEGIN *IGNORED.*: *([\w._:-]+) *--&gt;".
     * </pre>For example:<pre>
     * "&lt;-- BEGIN IGNORED : myLinesExample --&gt;".
      @see java.util.regex
     * 
     * @since JDynamiTe 1.1
     */
    public static String HTML_BEGIN_IGNORED_TAG = "\\s*<!-- *BEGIN *IGNORED.*: *([\\w._:\\-]+) *-->";
    /**
     * HTML default "End Ignored Element" tag.
     * This regular expression is : <pre>
     * "&lt;-- *END *IGNORED.*--&gt;".
     * </pre>For example:<pre>
     * "&lt;-- END IGNORED : myList --&gt;".
      @see java.util.regex
     * 
     * @since JDynamiTe 1.1
     */
    public static String HTML_END_IGNORED_TAG = "\\s*<!-- *END *IGNORED.*-->";
    
    /** Example of tag definition for a template written in XML language.
     * Use {@link #setVariableRegExp(String)} to change the default value. */
    public static String XML_VARIABLE_TAG = "\\{([\\w._:\\-]+)\\}";
    /** Example of tag definition for a template written in XML language.
     * Use {@link #setDynamicBeginRegExp(String)} to change the default value. */
    public static String XML_BEGIN_DYNAMIC_TAG = "<!-- *BEGIN *DYNAMIC.*: *([\\w._:\\-]+) *-->";
    /** Example of tag definition for a template written in XML language.
     * Use {@link #setDynamicEndRegExp(String)} to change the default value. */
    public static String XML_END_DYNAMIC_TAG = "<!-- *END *DYNAMIC.*-->";
    /** Example of tag definition for a template written in a "script" language.
     * Use {@link #setVariableRegExp(String)} to change the default value. */
    public static String SCRIPT_VARIABLE_TAG = "\\$([\\w._:\\-]+)";
    /** Example of tag definition for a template written in a "script" language.
     * Use {@link #setDynamicBeginRegExp(String)} to change the default value. */
    public static String SCRIPT_BEGIN_DYNAMIC_TAG = "#+ *BEGIN *DYNAMIC.*: *([\\w._:\\-]+) *";
    /** Example of tag definition for a template written in a "script" language.
     * Use {@link #setDynamicEndRegExp(String)} to change the default value. */
    public static String SCRIPT_END_DYNAMIC_TAG = "#+ *END *DYNAMIC.*";

    public static final String XML_UNDEFINED_INPUT = "___Undefined_File_Name___";
    
    public DefaultAnalyser() {  		
    }

    public void analyse(ITemplateDocument rootDocument, IDynamicElement rootElem, BufferedReader inputText) throws IOException {
        rootDoc = rootDocument;
        input = inputText;
        initRegExp();
        doAnalyse(rootElem, 0);
    }

    public Pattern getVariableRegExpPattern() {
        return varRegExpPattern;
    }
    public void setVariableRegExp(String variableRegExp) {
        varRegExpPattern = Pattern.compile(variableRegExp); 
    }
    public Pattern getDynamicBeginRegExpPattern() {
        return dynBeginRegExpPattern;
    }
    public void setDynamicBeginRegExp(String dynamicBeginRegExp) {
        dynBeginRegExpPattern = Pattern.compile(dynamicBeginRegExp);
    }

    public Pattern getDynamicBeginXMLRegExpPattern() {
        return dynBeginRegExpPattern;
    }
    public void setDynamicBeginXMLRegExp(String dynamicBeginXMLRegExp) {
        dynBeginXMLRegExpPattern = Pattern.compile(dynamicBeginXMLRegExp);
    }

    public Pattern getXMLVariableRegExpPattern() {
        return xmlVarRegExpPattern;
    }
    public void setXMLVariableRegExp(String variableRegExp) {
    	xmlVarRegExpPattern = Pattern.compile(variableRegExp); 
    }

    public Pattern getDynamicEndRegExpPattern() {
        return dynEndRegExpPattern;
    }
    public void setDynamicEndRegExp(String dynamicEndRegExp) {
        dynEndRegExpPattern = Pattern.compile(dynamicEndRegExp);
    }

    public Pattern getIgnoredBeginRegExpPattern() {
        return ignBeginRegExpPattern;
    }
    public void setIgnoredBeginRegExp(String ignoredBeginRegExp) {
        ignBeginRegExpPattern = Pattern.compile(ignoredBeginRegExp);
    }

    public Pattern getIgnoredEndRegExpPAttern() {
        return ignEndRegExpPattern;
    }
    public void setIgnoredEndRegExp(String ignoredEndRegExp) {
        ignEndRegExpPattern = Pattern.compile(ignoredEndRegExp);
    }

    public boolean getDebug() {
        return debug;
    }
    public void setDebug(boolean debugMode) {
        debug = debugMode;
    }

    private void initRegExp() {
        varRegExpPattern = null;
        dynBeginRegExpPattern = null;
        dynBeginXMLRegExpPattern = null;
        xmlVarRegExpPattern = null;
        dynEndRegExpPattern = null;
        ignBeginRegExpPattern = null;
        ignEndRegExpPattern = null;
               
        setVariableRegExp(DefaultAnalyser.HTML_VARIABLE_TAG);
        setDynamicBeginRegExp(DefaultAnalyser.HTML_BEGIN_DYNAMIC_TAG);
        setDynamicEndRegExp(DefaultAnalyser.HTML_END_DYNAMIC_TAG);
        setDynamicBeginXMLRegExp(DefaultAnalyser.HTML_BEGIN_XMLINPUT_DYNAMIC_TAG);
        setXMLVariableRegExp(DefaultAnalyser.HTML_XMLINPUT_VARIABLE_TAG);
        setIgnoredBeginRegExp(DefaultAnalyser.HTML_BEGIN_IGNORED_TAG);
        setIgnoredEndRegExp(DefaultAnalyser.HTML_END_IGNORED_TAG);
    }


    private void addMixed(IDynamicElement dynElem, StringBuffer content) {
        int normalVarIndex, xmlVarIndex;
        int normalVarEnd, xmlVarEnd;
        int varIndex, endIndex;
        String normalKey, xmlKey;
        boolean normalElemElected, xmlElemElected;
    	Matcher normalMatchVars = varRegExpPattern.matcher(content);
    	Matcher matchXMLVars = xmlVarRegExpPattern.matcher(content);
        boolean endOfSearch = false;
        int searchIndex = 0;

        while (!endOfSearch) {
        	normalVarIndex = xmlVarIndex = normalVarEnd = xmlVarEnd = -1;
        	normalElemElected = xmlElemElected = false;
            // Important: the variable name (the key) is the word which matches the first
            // "regexp" subexpression. That's why this word should be enclosed within brackets in varRegExp.
        	if (normalMatchVars.find(searchIndex)) {
        		normalVarIndex = normalMatchVars.start();
        		normalVarEnd = normalMatchVars.end();
        	}
        	if (dynElem.isXMLDynElement() && matchXMLVars.find(searchIndex)) {
        		xmlVarIndex = matchXMLVars.start();
        		xmlVarEnd = matchXMLVars.end();
        	}
        	if (normalVarIndex == -1 && xmlVarIndex == -1) {
        		break; // no more variable element
        	}
        	if (xmlVarIndex == -1) { // only a "normal" VariableElement was found
        		normalElemElected = true;
        	}
        	else if (normalVarIndex == -1) { // only an XML VariableElement was found
        		xmlElemElected = true;
        	}
        	else { // both VariableElement were found
        		if (normalVarIndex < xmlVarIndex)
        			normalElemElected = true;
        		else
        			xmlElemElected = true;
        	}
        	
            ITemplateElement varElem;
        	if (normalElemElected) {
        		normalKey = normalMatchVars.group(1);
        	    varElem = new VariableElement(normalKey);
        		varIndex = normalVarIndex;
        		endIndex = normalVarEnd;
                // Add this variable to the root document hashtable
        		rootDoc.setVariable(normalKey, "");
        	}
        	else { // xmlElemElected must be true ... but do this check: 
        		if (!xmlElemElected)
        			System.err.println("Internal Warning: unconsistant state in addMixed() method !");
        		xmlKey = matchXMLVars.group(1);
        		varElem = new XMLVariableElement(xmlKey, dynElem);
        		varIndex = xmlVarIndex;
        		endIndex = xmlVarEnd;
                // Add this variable to the root document hashtable
        		rootDoc.setVariable(xmlKey, "");
        	}
        	// First add the current "constant" section if any 
        	if (searchIndex < varIndex) {
        		ConstantElement constElem = new ConstantElement(content.substring(searchIndex,
        				                                                          varIndex));
        		dynElem.addElement(constElem);
        	}
        	// Then add the var. element 
            dynElem.addElement(varElem);
            // Add this variable to the root document hashtable
    		searchIndex = endIndex; 
        }
        // add remaining "constant" section if any
        if (searchIndex < content.length()) {
            ConstantElement constElem= new ConstantElement(content.substring(searchIndex));
            dynElem.addElement(constElem);
        }
        // clear content of mixed element passed by doAnalyse
        content.delete(0, content.length());
    }

   
    protected void doAnalyse(IDynamicElement dynElem, int depth) {
        StringBuffer mixed = new StringBuffer();
        boolean inIgnoredElement = false;
        boolean theEnd = false;
        while (!theEnd) {
            String line = null;
            try {
                line = input.readLine();
            }
            catch (IOException e) {
                System.err.println(e.getMessage());
                break;
            }
            if (line == null) {
                addMixed(dynElem, mixed);
                break;
            }
            Matcher matcher;
            if (inIgnoredElement) {
            	matcher = ignEndRegExpPattern.matcher(line);
                if (matcher.lookingAt()) {
                    // end of an ignored element
                    inIgnoredElement = false;
                    if (debug) {
                        System.err.println("<<End Ignore\n");
                    }
                }
                else if (debug) {
                    System.err.println("ignore[" + line + "]\n");
                }
                continue;
            }
            matcher = dynBeginXMLRegExpPattern.matcher(line);
            if (matcher.lookingAt()) {
                // begin of a XML dynamic element.
                // first add the current mixed element
                addMixed(dynElem, mixed);
                // then build a new dynamic element
                String name = matcher.group(1); // first subexpression
                if (name == null) {
                    System.err.println("Can not get dynamic element name from this line: " + line);
                    name = "___Undefined Name___";
                }
                String xmlFile = matcher.group(2); // second subexpression
                if (xmlFile == null) {
                    //System.err.println("Can not get XML input file name from this line: " + line);
                    xmlFile = DefaultAnalyser.XML_UNDEFINED_INPUT;
                }
                String xmlExpression = matcher.group(3); // third subexpression
                if (xmlExpression == null) {
                    System.err.println("Can not get XML XPath expression from this line: " + line);
                    xmlExpression = "___Undefined Expression___";
                }
                DefaultDynamicElement newDyn = new DefaultDynamicElement(name, xmlFile, xmlExpression, dynElem, this, rootDoc);
                dynElem.addElement(newDyn);
                rootDoc.recordDynElem(name, newDyn); // record it in ITemplateDocument root document
                doAnalyse(newDyn, depth + 1); // recursively analyse this new bloc
                continue;
            }
            matcher = dynBeginRegExpPattern.matcher(line);
            if (matcher.lookingAt()) {
                // begin of a dynamic element.
                // first add the current mixed element
                addMixed(dynElem, mixed);
                // then build a new dynamic element
                String name = matcher.group(1); // first subexpression
                if (name == null) {
                    System.err.println("Can not get dynamic element name from this line: " + line);
                    name = "___Undefined Name___";
                }
                DefaultDynamicElement newDyn = new DefaultDynamicElement(name, dynElem, this, rootDoc);
                dynElem.addElement(newDyn);
                rootDoc.recordDynElem(name, newDyn); // record it in ITemplateDocument root document
                doAnalyse(newDyn, depth + 1); // recursively analyse this new bloc
                continue;
            }
            matcher = dynEndRegExpPattern.matcher(line);
            if (matcher.lookingAt()) {
            	// end of the dynamic element
            	addMixed(dynElem, mixed);
            	theEnd = true;
            }
            else {
            	matcher = ignBeginRegExpPattern.matcher(line);
            	if (matcher.lookingAt()) {
            		// begin of an ignored element
            		inIgnoredElement = true;
            		if (debug) {
            			System.err.println("Begin Ignore>>\n");
            		}
            	}
            	else {
            		// just a line in the dynamic element
            		mixed.append(line + '\n');
            	}
            }
        }
    }
}
