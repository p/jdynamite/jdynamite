/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.analyser;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
/////import java.util.Hashtable;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Provides useful methods to create XML Document (org.w3c.dom.Document), evaluate XPath expressions, ...
 * Manages a "cache" list of already loaded Documents, in order to optimize performances.
 *
 */
public class XMLUtil {

	private static boolean m_verbose = false;
	
	// Elements stored in the cache list.
	private static class DocElem {
		String path;  // XML filename
		Document domDoc; // XML Document
		DocElem(String path, Document domDoc) {
			this.path = path;
			this.domDoc = domDoc;
		}
	}
	// FIFO to manage the cache list (thread safe)
	private static Queue<DocElem> m_XMLDocFifoCache = new ConcurrentLinkedQueue<DocElem>();
	private static int m_CacheMaxSize = 10;

	private static Document getDocument(String xmlFileName) {
		for (DocElem docElem  : m_XMLDocFifoCache) {
			if (docElem.path.equals(xmlFileName))
				return docElem.domDoc;
		}
		return null;
	}
	
	private static void addDocument(String xmlFileName, Document xmlDoc) {
		if (m_XMLDocFifoCache.size() >= m_CacheMaxSize) {
			// "cache" list already full
			if (XMLUtil.m_verbose)
				System.err.println("Cache max size reached. Removing from cache list: " + m_XMLDocFifoCache.element().path);
			m_XMLDocFifoCache.remove(); // need to remove the First In element
		}
		m_XMLDocFifoCache.offer(new XMLUtil.DocElem(xmlFileName, xmlDoc));
	}
	
	/**
	 * Gets the Documents cache list current size.
	 * @return Cache list current size (number of stored Documents)
	 */
	public static int getCacheSize() {
		return m_XMLDocFifoCache.size();
	}

	/**
	 * Gets the Documents cache list capacity.
	 * @return Cache list capacity (max number of stored Documents)
	 */
	public static int getCacheMaxSize() {
		return m_CacheMaxSize;
	}

	/**
	 * Sets the Documents cache list capacity.
	 * If new size if less than current size, then elements are removed (respecting FIFO order).
	 * @param cacheMaxSize (must be > 0)
	 */
	public static void setCacheMaxSize(int cacheMaxSize) {
		if (XMLUtil.m_verbose)
			System.err.println("XMLUtil.cacheMaxSize(" + cacheMaxSize + ')');
		if (cacheMaxSize <= 0)
			return;
		m_CacheMaxSize = cacheMaxSize;
		while(m_XMLDocFifoCache.size() > m_CacheMaxSize) {
			if (XMLUtil.m_verbose)
				System.err.println("Cache max size downsized. Removing from cache list: " + m_XMLDocFifoCache.element().path);
			m_XMLDocFifoCache.remove(); // need to remove the First In element
		}
	}
	
	/**
	 * Removes all elements of the Documents cache list.
	 */
	public static void resetCache() {
		if (XMLUtil.m_verbose)
			System.err.println("XMLUtil.resetCache()");
		m_XMLDocFifoCache.clear();
	}

	/**
	 * Creates an XML Document (org.w3c.dom.Document) from its file name.
	 * Once created, this Document reference is added to an internal list (a "cache").
	 * If a subsequent call to this method is done with the same filename, and if "forceReload" param
	 * is false, then the already created Document is returned. This is an optimization useful when
	 * it is assumed that the XML file did not change.
	 * @param xmlFileName XML file path.
	 * @param forceReload If true, creates and returns a new Document, even if it was previously created.
	 * @return the Document or null in case of error.
	 */
	public static Document initXMLdocument(String xmlFileName, boolean forceReload) {      
        Document xmlDocument;

//        System.err.println("XMLUtil.initXMLdocument(), xmlFileName=\"" + xmlFileName
//        		            + "\", forceReload=" + forceReload);
        if (!forceReload) {
        	xmlDocument = getDocument(xmlFileName);
        	if (xmlDocument != null) {
        		if (XMLUtil.m_verbose)
        			System.err.println("XMLUtil.initXMLdocument(), using xmlFileName=\"" + xmlFileName
        					+ "\" already loaded");
        		return xmlDocument;
        	}
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

		if (XMLUtil.m_verbose)
			System.err.println("XMLUtil.initXMLdocument(), loading xmlFileName=\"" + xmlFileName
					+ "\" ...");

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
			return null;
		}
        builder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                if (XMLUtil.m_verbose)
                	System.err.println("Ignoring " + publicId + ", " + systemId);
                return new InputSource(new StringReader(""));
            }
        });
		try {
			if (xmlFileName.indexOf(':') == -1) {
				// No ':' in the name ==> this is a 'simple local' file name
				xmlDocument = builder.parse(new File(xmlFileName));
			}
			else {
				// let's assume this is a URI ressource name ==> give it as it is to builder.parse
				// if (XMLUtil.verbose) System.err.println("XMLUtil.initXMLdocument(), URI= + xmlFileName);
				xmlDocument = builder.parse(xmlFileName);
			}
		} catch (SAXException e1) {
			e1.printStackTrace();
			return null;
		} catch (IOException e1) {
			//e1.printStackTrace();
			System.err.println("Can not open '" + xmlFileName + "'");
			File pwd = new File(".");
			try {
				System.err.println("Working dir. = " + pwd.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		if (xmlDocument != null) {
			addDocument(xmlFileName, xmlDocument);
		}
		return xmlDocument;
	}

	/**
	 * Evaluates an XPath expression in the specified context and return the result as the NodeList type.
	 * @param elem The starting context (a node, for example).
	 * @param expression The XPath expression.
	 * @return Result of evaluating the XPath expression as an NodeList.
	 * @see javax.xml.xpath.XPath#evaluate(String expression, Object item, QName returnType)
	 */
	public static NodeList evaluateExpression(Object elem, String expression) {
		NodeList liste = null;
		//System.err.println("XMLUtil.evaluateExpression(), expr=" + expression);
		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			liste = (NodeList) xpath.evaluate(expression, elem,
					XPathConstants.NODESET);
		} catch (XPathExpressionException xpee) {
			System.err.println(xpee.getMessage());
			//xpee.printStackTrace();
		}
		return liste;
	}

	/**
	 * Evaluates an XPath expression in the specified context and return the result as a String.
	 * @param elem The starting context (a node, for example).
	 * @param expression The XPath expression.
	 * @return The String that is the result of evaluating the expression and converting the result to a String.
	 * @see javax.xml.xpath.XPath#evaluate(String expression, Object item)
	 */
	public static String evaluateExpressionAsString(Object elem, String expression) {
		String value = null;
		//System.err.println("XMLUtil.evaluateExpression(), expr=" + expression);
		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			value = xpath.evaluate(expression, elem);
		} catch (XPathExpressionException xpee) {
			System.err.println(xpee.getMessage());
			//xpee.printStackTrace();
		}
		return value;
	}

	public static void setVerbose(boolean isVerbose) {
		XMLUtil.m_verbose = isVerbose;
	}
	
	public static void main(String[] args) {
		String xpathExpr;
		int cacheSize;
		
		if (args.length < 3) {
			System.err.println("\nUsage: XMLUtil XPathExpression cacheSize inputXMLFileName ...\n");
			return;
		}
		xpathExpr = args[0];
		cacheSize = Integer.parseInt(args[1]);
		System.err.println("xpathExpr=" + xpathExpr + ", cacheSize=" + cacheSize);
		for (int i = 2; i < args.length; i++) System.err.println("XMLFile=" + args[i]);
		
		XMLUtil.setVerbose(true);
		XMLUtil.setCacheMaxSize(cacheSize);
		
		for (int loop = 0; loop < 4; loop++) {
			System.err.println("Loop #" + loop);
			if (loop == 2) { // test resetCache
				XMLUtil.resetCache();
			}
			else if (loop == 3) { // test resetCache
				XMLUtil.setCacheMaxSize(2);
			}
			for (int fileIndex = 2; fileIndex < args.length; fileIndex++) {
				String fileName = args[fileIndex];
				Document xmlDoc = XMLUtil.initXMLdocument(fileName, false);
				System.err.println("   --- current cache size:" + XMLUtil.getCacheSize());
				if (xmlDoc != null) {
					NodeList nodeList = XMLUtil.evaluateExpression(xmlDoc, xpathExpr);
					if (nodeList != null) {
						for (int i = 0; i < nodeList.getLength(); i++) {
							Node node = nodeList.item(i);
							NamedNodeMap nodeMap = node.getAttributes();
							System.out.println("node# " + i);
							String content = node.getTextContent();
							int nbCharToPrint = content.length() < 80 ? content.length() : 80; 
							System.out.println("  node.getTextContent(): " + content.substring(0, nbCharToPrint) + " ...");
							if (nodeMap == null) {
								System.err.println("  XML nodeMap is null !!!");
								continue;
							}
							for (int a=0; a < nodeMap.getLength(); a++) {
								Node attrNode = nodeMap.item(a);
								System.out.println("    attribute# " + a);
								System.out.println("      name: " + attrNode.getNodeName());
								System.out.println("      value: " + attrNode.getNodeValue());
							}
						}
					}   		
				}
			}
		}
	}
}
