/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.analyser;

import cb.jdynamite.ITemplateDocument;


public interface IDynamicElement {
    public void setValue(String value);
    public void parse(ITemplateDocument rootDocument);
    public void parseXML(ITemplateDocument rootDocument, boolean recurse);
    public void addElement(ITemplateElement templateElement);
    public IDynamicElement getFatherDynElem();
    public String getName();
    // TODO: define a specific interface (IXMLDynamicElement) for the method below ???
    public boolean isXMLDynElement();
    public String getXMLValue(String key);
    public boolean hasXMLElement();
    public void setXMLInput(String value_URI);
    public String getXMLInput();
    public void setXMLXPathExpression(String xPathExpr);
    public String getXMLXPathExpression();
	public Object getXMLCurrentNode();
}
