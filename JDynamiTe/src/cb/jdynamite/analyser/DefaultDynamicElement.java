/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite.analyser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//// import java.util.Hashtable;

//// import org.w3c.dom.Document;
//// import org.w3c.dom.NamedNodeMap;
//// import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cb.jdynamite.ITemplateDocument;

public class DefaultDynamicElement implements ITemplateElement, IDynamicElement {
    private StringBuffer value;
    private ArrayList<ITemplateElement> templateDef;
    private String name; // just for debug
    private IDynamicElement fatherElem;
    private ITemplateAnalyser templateAnalyser;
    private ITemplateDocument templateRootDocument; // JDynamiTe object a priori
    // XML specific
    private String xmlXPathExpr;
    private String xmlInput;
    private boolean xmlIsRelativeXPathExpression;
    private String xmlVariableKeyInXPathExpression;
    private String xmlVariableKeyInXMLInput;
    private String simpleVariableKeyInXMLInput;
    private Pattern xmlVarRegExpPattern;
    private Pattern simpleVarRegExpPattern;
	private NodeList xmlNodeList;
	private boolean xmlInitToDo = false;
	private boolean xmlIam = false;
	private int xmlCurrentNode = 0;

    public DefaultDynamicElement() {
    	templateDef = new ArrayList<ITemplateElement>();
    	value = new StringBuffer();
    }

    public DefaultDynamicElement(String elementName, IDynamicElement dynFatherElem, ITemplateAnalyser analyser, ITemplateDocument rootDocument) {
        this();
        name = elementName;
        fatherElem = dynFatherElem;
        templateAnalyser = analyser;
        templateRootDocument = rootDocument;
    }
    
    public DefaultDynamicElement(String elementName, String xmlInputFile, String xmlXPathEpression, IDynamicElement dynFatherElem, ITemplateAnalyser analyser, ITemplateDocument rootDocument) {
        this();
        name = elementName;
        fatherElem = dynFatherElem;
        templateAnalyser = analyser;
        templateRootDocument = rootDocument;
        // XML specific part:
        xmlIam = true;
    	xmlVarRegExpPattern = templateAnalyser.getXMLVariableRegExpPattern(); 
    	simpleVarRegExpPattern = templateAnalyser.getVariableRegExpPattern();
        setXMLInput(xmlInputFile);
        setXMLXPathExpression(xmlXPathEpression);
        xmlInitToDo = true;
        xmlCurrentNode = 0;
    }

    public void resetDefinition() {
    	templateDef.clear();
    	value.delete(0, value.length());
    	xmlIam = false;
    	xmlInitToDo = false;
    	xmlCurrentNode = 0;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getValue(ITemplateDocument rootDocument) {
        return value.toString();
    }

    public void setValue(String arbitraryValue) {
        value.delete(0, value.length());
        if (arbitraryValue != null) {
            value.append(arbitraryValue);
        }
        if (xmlIam) {
        	xmlCurrentNode = 0;
        	if (xmlIsRelativeXPathExpression || simpleVariableKeyInXMLInput != null || 
        		xmlVariableKeyInXMLInput != null || xmlVariableKeyInXPathExpression != null)
        		// in this case, the current XML node of xmlFatherElem may have changed, so
        		// we must re-evaluate the xml attribute values.
        		xmlInitToDo = true;
        }
    }
    
    public String getDefinition(int depth) {
        StringBuffer def = new StringBuffer();
        for (int indent = 0; indent < depth; indent++) {
            def.append("   ");
        }
        def.append("DefaultDynamicElement \"" + name + "\":");
        if (xmlIam) {
        	def.append(" (XML input=" + xmlInput + ", XML Expression=" + xmlXPathExpr + ')');
        	if (simpleVariableKeyInXMLInput != null) def.append(" simpleVarInputKey=" + simpleVariableKeyInXMLInput);
        	if (xmlVariableKeyInXMLInput != null) def.append(" xmlVarInputKey=" + xmlVariableKeyInXMLInput);
        	if (xmlVariableKeyInXPathExpression != null) def.append(" varExprKey=" + xmlVariableKeyInXPathExpression);
        }
        def.append('\n');
        for (ITemplateElement elem : templateDef) {
            def.append(elem.getDefinition(depth + 1));        	
        }
        return def.toString();
    }

    @Override
	public IDynamicElement getFatherDynElem() {
		return fatherElem;
	}

    public void parse(ITemplateDocument rootDocument) {
    	//System.err.println("DefaultDynamicElement.parse(), name=" + name);
    	if (xmlIam && xmlInitToDo) {
    		initxmlNodeValues();
    		xmlInitToDo = false;
    	}
        for (ITemplateElement elem : templateDef) {
            value.append(elem.getValue(rootDocument));
        }
        if (xmlIam)
        	xmlCurrentNode++;
    }

    public void parseXML(ITemplateDocument rootDocument, boolean recurse) {
    	//System.err.println("DefaultDynamicElement.parse(), name=" + name + ", recurse=" + recurse);
    	while (hasXMLElement()) {
            for (ITemplateElement elem : templateDef) {
    			if (recurse) {
    				if (elem instanceof IDynamicElement) {
    					IDynamicElement dynElem = (IDynamicElement)elem;
    					dynElem.setValue("");
    					dynElem.parseXML(rootDocument, recurse);
    				}
    			}
    			value.append(elem.getValue(rootDocument));
    		}
    		xmlCurrentNode++;
    	}
    }
    
    public void addElement(ITemplateElement templateElement) {
    	templateDef.add(templateElement);
    }

	@Override
	public boolean isXMLDynElement() {
		return xmlIam;
	}

	@Override
	public String getXMLValue(String key) {
    	if (xmlIam && !xmlInitToDo && xmlNodeList != null && xmlCurrentNode < xmlNodeList.getLength()) {
    //		String value = xmlNodeValues.get(xmlCurrentNode).get(key);
    		String value = XMLUtil.evaluateExpressionAsString(getXMLCurrentNode(), key);
    		return value;
        }
		return null;
	}

	protected String getXMLVariableKeyFromString(String strValue) {
    	Matcher matchXMLVars = xmlVarRegExpPattern.matcher(strValue);
    	if (matchXMLVars.find()) {
    		return matchXMLVars.group(1);
    	}
    	return null;
	}

	protected String getSimpleVariableKeyFromString(String strValue) {
    	Matcher matchVars = simpleVarRegExpPattern.matcher(strValue);
    	if (matchVars.find()) {
    		return matchVars.group(1);
    	}
    	return null;
	}

	@Override
    public void setXMLInput(String value_URI) {
        xmlInput = value_URI;
        if (xmlInput.equals(DefaultAnalyser.XML_UNDEFINED_INPUT)) {
        	xmlIsRelativeXPathExpression = true;
        	simpleVariableKeyInXMLInput = null;
        	xmlVariableKeyInXMLInput = null;
        }
        else {
        	xmlIsRelativeXPathExpression = false;
        	xmlVariableKeyInXMLInput = getXMLVariableKeyFromString(xmlInput);
        	if (xmlVariableKeyInXMLInput == null)
        		simpleVariableKeyInXMLInput = getSimpleVariableKeyFromString(xmlInput);
        	else
        		simpleVariableKeyInXMLInput = null;
        }
        xmlInitToDo = true;
	}

	@Override
    public String getXMLInput() {
		return xmlInput;
	}

	@Override
	public void setXMLXPathExpression(String xPathExpr) {
        xmlXPathExpr = xPathExpr;		
		xmlVariableKeyInXPathExpression = getXMLVariableKeyFromString(xmlXPathExpr);
        xmlInitToDo = true;
	}

	@Override
	public String getXMLXPathExpression() {
		return xmlXPathExpr;
	}

	@Override
    public boolean hasXMLElement() {
		if (!xmlIam)
			return false;
    	if (xmlInitToDo) {
    		initxmlNodeValues();
    		xmlInitToDo = false;
    	}
		return (xmlNodeList != null && xmlCurrentNode < xmlNodeList.getLength());
	}

	@Override
	public Object getXMLCurrentNode() {
		if (xmlIam && xmlNodeList != null && xmlCurrentNode < xmlNodeList.getLength()) 
			return xmlNodeList.item(xmlCurrentNode);
		else
			return null;
	}

    private void initxmlNodeValues() {
    	Object rootObject = null;
    	String xPathExpr = xmlXPathExpr;
    	String lastError = null;
    	String usedXmlInput = null;
    	
    	templateRootDocument.setLastError(lastError);
    	if (fatherElem == null)  {
    		// We are the JDynamiTe root element ! Can not normally happen because 
    		// this element is not an XML one.
    		lastError = "XML function on JDynamiTe root element !!!";
    		System.err.println("Error: " + lastError);
        	templateRootDocument.setLastError(lastError);
    		xmlNodeList = null;
    		return;
    	}
    	// First, determine the rootObject
    	if (xmlIsRelativeXPathExpression) {
    		rootObject = fatherElem.getXMLCurrentNode();
    		if (rootObject == null) {
    			lastError = "Can not get XML input context for this element (" + name +
    					") from parent element (" + fatherElem.getName() + ")";
        		System.err.println("Error: " + lastError);
    		}
    	}
    	else if (simpleVariableKeyInXMLInput != null) { // does XML input string contain a (simple) Variable ? 
        	String varValue = templateRootDocument.getVariable(simpleVariableKeyInXMLInput);
        	if (varValue != null && !varValue.isEmpty()) {
        		// replace the variableKey in xmlInput by its current value
            	Matcher matchVars = simpleVarRegExpPattern.matcher(xmlInput);
            	// see Matcher.quoteReplacement() javadoc:
            	// "The String produced will match the sequence of characters
            	// treated as a literal sequence. Slashes ('\') and dollar signs ('$')
            	// will be given no special meaning."
            	// ==> We need to use it here to accept Windows path with '\'
            	String literalString = Matcher.quoteReplacement(varValue);
            	varValue = matchVars.replaceFirst(literalString);
            	usedXmlInput = varValue;
            	rootObject = XMLUtil.initXMLdocument(usedXmlInput, false);
        	}
        	else {
    			lastError = "Can not get value to build XML input for this element (" + name +
    					") from var key (" + simpleVariableKeyInXMLInput+ ")";
        		System.err.println("Error: " + lastError);
    		}
    	}
    	else if (xmlVariableKeyInXMLInput != null) { // is XML input string an XML Variable ?
        	String varValue = fatherElem.getXMLValue(xmlVariableKeyInXMLInput);
        	if (varValue != null && !varValue.isEmpty()) {
        		// replace the XML variableKey in xmlInput by its current value
            	Matcher matchVars = xmlVarRegExpPattern.matcher(xmlInput);
            	// see Matcher.quoteReplacement() javadoc:
            	// "The String produced will match the sequence of characters
            	// treated as a literal sequence. Slashes ('\') and dollar signs ('$')
            	// will be given no special meaning."
            	// ==> We need to use it here to accept Windows path with '\'
            	String literalString = Matcher.quoteReplacement(varValue);
            	varValue = matchVars.replaceFirst(literalString);
            	usedXmlInput = varValue;
            	rootObject = XMLUtil.initXMLdocument(usedXmlInput, false);
        	}
        	else {
    			lastError = "Can not get value to build XML input for this element (" + name +
    					") from xml var key (" + xmlVariableKeyInXMLInput + 
    					") from parent element context (" + fatherElem.getName() + ")";
        		System.err.println("Error: " + lastError);
    		}
    	}
    	else {
    		usedXmlInput = xmlInput;
    		rootObject = XMLUtil.initXMLdocument(xmlInput, false);
    	}
    	// Second, evaluate xPathExpr if it contains an XML Variable
    	if (xmlVariableKeyInXPathExpression != null) {
        	String xmlVarExprValue = fatherElem.getXMLValue(xmlVariableKeyInXPathExpression);
        	if (xmlVarExprValue != null && !xmlVarExprValue.isEmpty()) {
        		// replace the variableKey by its current value
            	Matcher matchXMLVars = xmlVarRegExpPattern.matcher(xmlXPathExpr);
        		xPathExpr = matchXMLVars.replaceFirst(xmlVarExprValue);
        	}
        	else {
    			lastError = "Can not get value to build XPath expr. for this element (" + name +
    					") from xml var key (" + xmlVariableKeyInXPathExpression + 
    					") from parent element context (" + fatherElem.getName() + ")";
        		System.err.println("Error: " + lastError);
    		}
    	}
    	xmlNodeList = null;
    	if (rootObject != null) {
        	xmlNodeList = XMLUtil.evaluateExpression(rootObject, xPathExpr);
    	}
    	else {
    		lastError = "Can not get XML value for this template element: " + name;
    		if (usedXmlInput != null)
    			lastError += " --- possible cause: can not open XML input file \"" + usedXmlInput + '\"';
    		System.err.println("Error: " + lastError);
        }    		
    	templateRootDocument.setLastError(lastError);
    }

}
