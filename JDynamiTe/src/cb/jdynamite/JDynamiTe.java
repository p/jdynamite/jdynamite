/*
 *  JDynamiTe - Dynamic Template in Java
 *  Copyright (C) 2001, 2002, 2014, Christophe Bouleau
 *
 *  This file is part of JDynamiTe.
 * 
 *  JDynamiTe is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JDynamiTe is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JDynamiTe.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cb.jdynamite;

import java.util.*;
import java.io.*;
import cb.jdynamite.analyser.*;


/**
 * JDynamiTe is the unique class that you need in order to parse a template document, populate it and finally produce an output document.<br>
 * It is very simple to use: <br>
 * - First define the input file or input stream (for example an HTML template document)
 * with the "setInput" method.<br>
 * - Secondly fill and develop this document with a few methods such as "setVariable",
 * and "parseDynElem".<br>
 * - Finally, after calling the "parse" method to finalize building your dynamic document,
 * you simply obtain the result with the "toString" method.
 * <br><br>
 * There are several examples available from JDynamiTe Project Home page.
 * <br><br>
 * 
 * <h3 id="jdyn_elem">JDynamiTe elements</h3>
 * There are (only) five types of JDynamiTe elements. Inserted into a document, they make it a "template" document.
 * <br><br><b>1) "Variable Element"</b><br>
 * A "Variable Element" is defined by an identifier in the template document.
 * It is a simple variable, whose value can be changed with the {@link #setVariable(String, String)} method.<br>
 * In the generated output document, a Variable element is replaced by its value.<br>
 * Example of use: titles, values of cells in tables, etc.
 * <br><br><b>2) "Dynamic Element"</b><br>
 * A "Dynamic Element" is a "block" which can be dynamically developed.
 * It has an identifier, a template definition, and a current value.
 * Its value can be set or developed with the {@link #parseDynElem(String)} and {@link #setDynElemValue(String, String)} methods.<br>
 * Example of use: list, table, enumeration, etc.
 * <br><br><b>3) "XML Dynamic Element"</b><br>
 * Since JDynamiTe 2.0, there is a new kind of Dynamic Element: "XML Dynamic Element" allows to
 *  automatically populate the template taking input from XML files (see {@link #setXMLInput(String, String)}). It uses standard XPath syntax, 
 *  which refers to data (XML nodes) contained in an external XML file. By this way, no need to programmatically populate this block of data: 
 *  JDynamiTe parser automatically does it for you (see {@link #parseXMLDynElem(String, boolean)} or {@link #parseXMLDynElem()}).<br> 
 *  Several examples available in JDynamiTe Project Examples Home page.
 * <br><br><b>4) "XML Variable Element"</b><br>
 * An "XML Variable Element" is a simple variable in the current "XML Dynamic Element" context (i.e. relative to current XML node).<br>
 * It can be an attribute node or a text node<br>
 * In the generated output document, an XML Variable element is replaced by its value in its context.
 * <br><br><b>5) "Ignored Element"</b><br>
 * Since JDynamiTe 1.1, this optional element can be used in template documents. An "Ignored Element" is
 * a "block" which will be completely ignored (skipped during document analysis).<br>
 * Example of use: it allows the HTML designer to build more realistic pages, with concrete data, giving a
 * better idea of the possible dynamic page look and feel. An "Ignored Element" can be inserted anywhere in
 * the template document, even inside "Dynamic Element".
 * <br><br><br>
 * 
 * <h3 id="template_syntax">Template document syntax</h3>
 * Special tags allow to declare JDynamiTe elements (described above) in a template.<br>
 * Technically, tag syntax is defined by regular expressions (which can be customized if needed,
 * using {@code DefaultAnalyser} class methods). The default syntax is suitable for most usages.<br>
 * Below is a simplified (more readable than full regular expressions) definition of these tags.<br>
 * Note that the best way to learn how to insert JDynamiTe tags in a template document is probably to look at the
 * "JDynamiTe QuickStart example", which is the first (and simple) example provided in JDynamiTe Project Home page
 * (follow "JDynamiTe Examples Overview", the input template file is "testTemplateV2_0.html").<br>
 * <br>
 * <b>1) "Variable" tag</b>
 * <p><pre>{@literal {VARNAME}}</pre></p>
 * Defines {@code VARNAME} as a <b>Variable Element</b>.<br>
 * Note that you can use several times {VARNAME} in your template.
 * <br><br><br>
 * <b>2) "Begin Dynamic Element" tag</b>
 * <p><pre>{@literal <!-- BEGIN DYNAMIC : blockName -->}</pre></p>
 * Defines {@code blockName} as a <b>Dynamic Element</b>.<br>
 * Its definition begins at the next line of the template.<br>
 * Note that Dynamic Elements can be nested.
 * <br><br><br>
 * <b>3) "Begin XML Dynamic Element" tag</b>
 * <p><pre>{@literal <!-- BEGIN XML DYNAMIC : blockName [URI|filename] XPathExpression -->}</pre></p>
 * Defines {@code blockName} as an <b>XML Dynamic Element</b>.<br>
 * Its definition begins at the next line of the template.<br>
 * If {@code URI} or {@code filename} is specified, this "URI" or "filename" is read and XML data are extracted from it.<br>
 * Otherwise, XML data are extracted from parent XML Dynamic Element context (XML Dynamic Elements can be nested).<br>
 * <br>
 * "XPathExpression" is an "XPath compliant" expression which selects the current context (current XML node).<br>
 * It can be an "absolute" XML path (beginning with '/') or a relative XML path (it is relative to the current context).<br> 
 * See the Javadoc documentation of Package "javax.xml.xpath" or visit "http://www.w3.org/TR/xpath" for details and examples about XPath expressions.<br>
 * <br>
 * As said above, there are several examples of "XML Dynamic Element" usage in "JDynamiTe Examples Overview" pages. 
 * <br><br><br>
 * <b>4) "XML Variable" tag</b><br><br>
 * There are two kinds of "XML Variables". Both are enclosed by double braces:<br><br>
 * <ol>
 * <li>Attribute</li>
 * <br><pre>{@literal {{[RelativeXPath/]@NODE_ATTRIBUTE_NAME}}}</pre>
 * If {@code RelativeXPath/} is not specified, selects the attribute {@code NODE_ATTRIBUTE_NAME} of the current XML Dynamic Element (current XML node).<br>
 * If {@code RelativeXPath/} is specified, selects the attribute {@code NODE_ATTRIBUTE_NAME} of the current XML Dynamic Element child node, defined by the
 * path {@code RelativeXPath/}.<br><br>
 * <li>Text</li>
 * <p><pre>{@literal {{[RelativeXPath/]text()}}}</pre></p>
 * If {@code RelativeXPath/} is not specified, selects the "text" part of the current XML Dynamic Element (current XML node).<br>
 * If {@code RelativeXPath/} is specified, selects the "text" part of the current XML Dynamic Element child node, defined by the
 * path {@code RelativeXPath/}.
 * </ol> 
 * <br><br>
 * <b>5) "End Dynamic Element" tag</b>
 * <p><pre>{@literal <-- END DYNAMIC : blockName -->}</pre></p>
 * means that the Dynamic Element or XML Dynamic Element definition of "blockName" ends on the previous line.
 * <br><br><br>
 * <b>6) "Ignored Element" tags</b><br>
 * <p><pre>{@literal <!-- BEGIN IGNORED : blockName -->}</pre></p>
 * means that an ignored block (skipped during analysis and document output) begins on the next line.
 * <p><pre>{@literal <!-- END IGNORED : blockName -->}</pre></p>
 * means that this ignored block ends on the previous line.<br>
 * <br><br>
 * <b>7) Identifiers syntax (for Variable name and Dynamic Element name)</b><br>
 * By default, it is a word made up of alphanumeric characters and these four characters:
 * {@code '.', ':', '_' and '-'}
 *
 * <br><br><br>
 * <h3>JDynamiTe home page</h3>
 * JDynamiTe latest versions, news, examples ... at <b>http://jdynamite.sourceforge.net</b>
 * <br><br><br>
 * @author Christophe Bouleau
 */
public class JDynamiTe extends DefaultDynamicElement implements ITemplateDocument, Serializable {
    private static final long serialVersionUID = 1L;
    private static boolean verbose = false;
    private Hashtable<String, String> variables;
    private Hashtable<String, IDynamicElement> dynamics;
    private ITemplateAnalyser analyser;
    private String lastError;
/**
 * Special variable tag, {__LAST_JDYN_ERROR__}, that you can use in your
 * template, and which is automatically populated by JDynamiTe, after parsing Dynamic Elements,
 * with the last error message (it is empty if no error occurred). Errors can typically happen when
 * an external XML input document can not be open.
 * @see #getLastError()
 */
public static final String INPUT_TEMPLATE_ERROR_TAG = "__LAST_JDYN_ERROR__";
 
    /**
     * Constructs an empty "JDynamiTe" (Java Dynamic Template) document.
     */
    public JDynamiTe() {
        this("__topLevel__");
    }
    
    /**
     * Constructs an empty "JDynamiTe" (Java Dynamic Template) document.
     *
     * @param name The name of this "JDynamiTe" document, used for debugging purposes by the
     * "getTemplateDefinition" method.
     */
    public JDynamiTe(String name) {
        super(name, null, null, null);
        variables = new Hashtable<String, String>();
        dynamics = new Hashtable<String, IDynamicElement>();
        lastError = null;
        recordDynElem(name, this);
    }
    
    /**
     * Starts the analysis of the input template document.
     * If no custom "ITemplateAnalyser" was defined, JDynamiTe uses the "DefaultAnalyser".
     * After this step, all the Variables and Dynamic Elements are identified,
     * so this JDynamiTe is ready to be developed.
     * By default all Variables are set to an empty string value.
     * In addition, Variable identifiers (keys) can be retrieved by getVariablesKeys.
     *
     * @param reader A BufferedReader to read the input template document.
     * @exception java.io.IOException If an I/O error occurs.
     * @see #setInput(java.io.InputStream)
     * @see #setInput(java.lang.String)
     * @see cb.jdynamite.analyser.DefaultAnalyser
     * @see #setAnalyser(ITemplateAnalyser templateAnalyser)
     * @since JDynamyTe 1.2
     */
    public void setInput(BufferedReader reader) throws IOException {
        resetDefinition();
        variables.clear();
        dynamics.clear();
        recordDynElem(getName(), this);
        if (analyser == null) {
            analyser = new DefaultAnalyser();
        }
        analyser.analyse(this, this, reader);
    }
    
    /**
     * Starts the analysis of the input template document.
     * If no custom "ITemplateAnalyser" was defined, JDynamiTe uses the "DefaultAnalyser".
     * After this step, all the Variables and Dynamic Elements are identified,
     * so this JDynamiTe is ready to be developed.
     * By default all Variables are set to an empty string value.
     * In addition, Variable identifiers (keys) can be retrieved by getVariablesKeys.
     *
     * @param istream A stream to read the input template document.
     * @exception java.io.IOException If an I/O error occurs.
     * @see #setInput(java.io.BufferedReader)
     * @see #setInput(java.lang.String)
     * @see cb.jdynamite.analyser.DefaultAnalyser
     * @see #setAnalyser(ITemplateAnalyser templateAnalyser)
     */
    public void setInput(InputStream istream) throws IOException {
        setInput(new BufferedReader(new InputStreamReader(istream)));
    }

    /**
     * Starts the analysis of the input template document.
     * If no custom "ITemplateAnalyser" was defined, JDynamiTe uses the "DefaultAnalyser".
     * After this step, all the Variables and Dynamic Elements are identified,
     * so this JDynamiTe is ready to be developed.
     * By default all Variables are set to an empty string value.
     * In addition, Variable identifiers (keys) can be retrieved by getVariablesKeys.
     *
     * @param fileName The name of the file which contains the input template document.
     * @exception java.io.IOException If an I/O error occurs.
     * @exception java.io.FileNotFoundException Bad filename.
     * @see #setInput(java.io.BufferedReader)
     * @see #setInput(java.io.InputStream)
     * @see cb.jdynamite.analyser.DefaultAnalyser
     * @see #setAnalyser(ITemplateAnalyser templateAnalyser)
     */
    public void setInput(String fileName) throws IOException, FileNotFoundException {
        FileReader fileReader = new FileReader(fileName);
        setInput(new BufferedReader(fileReader));
        fileReader.close();
    }
    
    /**
     * Returns the value of a variable that was set by setVariable.
     * A "JDynamiTe" document maintains a list of key/value pairs, which is used when
     * Dynamic Elements are parsed.
     *
     * @param key The key (identifier) of the variable to retrieve.
     * @return The value of the variable, or null if this variable does not exist.
     * @see #setVariable(java.lang.String, java.lang.String)
     */
    public String getVariable(String key) {
        return variables.get(key);
    }
    
    /**
     * Sets the value of a variable.
     * Overrides its previous value if any.
     * A "JDynamiTe" document maintains a list of key/value pairs, which is used when
     * Dynamic Elements are parsed.
     *
     * @param key The key (identifier) of the variable to set.
     * @param value The new value of this variable.
     * @see #getVariable(java.lang.String)
     */
    public void setVariable(String key, String value) {
        variables.put(key, value);
    }
    
    /**
     * Returns an enumeration of the Variable keys (identifiers) in this JDynamiTe document.
     * A "JDynamiTe" document maintains a list of key/value pairs, which is used when
     * Dynamic Elements are parsed.
     * The setInput method builds this list (with an empty string as default value)
     * during the template document analysis. After this step, the setVariable method
     * is used to set a value to a Variable.
     * Example of use:
     * <pre>
     * Enumeration<String> keys = getVariableKeys();
     * while (keys.hasMoreElements()) {
     *    String key = keys.nextElement();
     *    System.err.println(keys);
     *    String value = myDatabase.selectValue(key); // get the current value somewhere...
     *    myJDynamiTeDoc.setVariable(key, value); // set the template Variable.
     * }
     * </pre>
     *
     * @return Enumeration of the Variable keys.
     * @see #setVariable(java.lang.String, java.lang.String)
     * @see #setInput(InputStream istream)
     * @see #setInput(String fileName)
     * @since JDynamiTe 1.2
     */
    public Enumeration<String> getVariableKeys() {
        return variables.keys();
    }
    
    /**
     * Returns the "Dynamic Element" identified by a string
     * (note: you do not need to directly call this method to use JDynamiTe).
     * A "JDynamiTe" document maintains a list of "Dynamic Elements".
     *
     * @param key The "Dynamic Element" identifier. This is the string defined in the
     * template document in the "BEGIN DYNAMIC" tag. For example:
     * <pre>&lt;-- BEGIN DYNAMIC : myList --&gt;</pre>
     * Here "myList" identifies the Dynamic Element which begins.
     * @return The interface of the Dynamic Element.
     */
    public IDynamicElement getDynElem(String key) {
        return dynamics.get(key);
    }

    /**
     * Returns an enumeration of the Dynamic Element keys (identifiers) in this JDynamiTe document.
     *
     * @return Enumeration of the Dynamic Element keys.
     * @see #setDynElemValue(String, String)
     * @see #parseDynElem(String)
     * @see #parseXMLDynElem(String, boolean)
     * @see #parseXMLDynElem()
     * @since JDynamiTe 1.2
     */
    public Enumeration<String> getDynElemenKeys() {
        return dynamics.keys();
    }

    /**
     * Determines if an XML Dynamic Element has still XML nodes to parse.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     *  
     * @param dynElemName "XML Dynamic Element" identifier.
     * @return false when all XML nodes of "dynElemName" have been parsed.
     * Returns true otherwise.
     * 
     * @see #isXMLDynElement(String)
     */
    public boolean hasXMLElement(String dynElemName) {
        IDynamicElement dynElem = getDynElem(dynElemName);
        if (dynElem != null) {
            return dynElem.hasXMLElement();
        }
        return false;
    }
    
    /**
     * Determines if a Dynamic Element is an XML Dynamic Element.
     * Some "XML" methods can be called on XML Dynamic Elements.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     * 
     * @param dynElemName Dynamic Element identifier 
     * @return true if dynElemName is the identifier of an XML Dynamic Element.
     * Returns false otherwise.
     * 
     * @see #hasXMLElement(String)
     * @see #setXMLInput(String, String)
     * @see #getXMLVariable(String, String)
     * @see #setXMLXPathExpression(String, String)
     */
    public boolean isXMLDynElement(String dynElemName) {
        IDynamicElement dynElem = getDynElem(dynElemName);
        if (dynElem != null) {
            return dynElem.isXMLDynElement();
        }
        return false;
    }
    
    /**
     * Sets or replaces XML input path (URI or filename) of an XML Dynamic Element.
     * Useful for example when an XML input path (in "Begin XML Dynamic Element" tag) is not defined in the parsed template file.
     * Does nothing on non XML Dynamic Elements.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     * 
     * @param xmlDynElemName "XML Dynamic Element" identifier.
     * @param value_URI new path
     * 
     * @see #isXMLDynElement(String)
     */
    public void setXMLInput(String xmlDynElemName, String value_URI) {
        IDynamicElement dynElem = getDynElem(xmlDynElemName);
        if (dynElem != null && dynElem.isXMLDynElement()) {
            dynElem.setXMLInput(value_URI);
        }
    }

    /**
     * Replaces XML XPath expression of an XML Dynamic Element.
     * Useful to programmatically  set the XML XPath expression (whose initial value is defined in "Begin XML Dynamic Element"
     *  tag of the template file).<br>
     * Does nothing on non XML Dynamic Elements.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     * 
     * @param xmlDynElemName "XML Dynamic Element" identifier.
     * @param xPathExpr new XML XPath expression
     * 
     * @see #isXMLDynElement(String)
     */
    public void setXMLXPathExpression(String xmlDynElemName, String xPathExpr) {
        IDynamicElement dynElem = getDynElem(xmlDynElemName);
        if (dynElem != null && dynElem.isXMLDynElement()) {
            dynElem.setXMLXPathExpression(xPathExpr);
        }
    }

    /**
     * Returns the value of an XML Variable in its in current "XML Dynamic Element" context.
     * It can be an attribute node or a text node (XML Variables are enclosed by double braces in "XML Variable" tag).<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.<br>
     * Does nothing on non XML Dynamic Elements.
     * 
     * @param xmlDynElemName
     * @param xmlVarName
     * 
     * @return Value of xmlVarName in its in current "XML Dynamic Element" context
     * 
     * @see #isXMLDynElement(String)
     */
    public String getXMLVariable(String xmlDynElemName, String xmlVarName) {
        IDynamicElement dynElem = getDynElem(xmlDynElemName);
        if (dynElem != null && dynElem.isXMLDynElement()) {
            return dynElem.getXMLValue(xmlVarName);
        }
        return null;
    }

    /**
     * Records a "Dynamic Element" in this "JDynamiTe" Document
     * (Note: You do not need to directly call this method to use JDynamiTe).
     * A "JDynamiTe" document maintains a list of "Dynamic Elements".
     * It is called by the analyser.
     * (all Dynamic Elements are put in the same list, so they must have a unique identifier).
     *
     * @param key The "Dynamic Element" identifier.
     * @param value An object that implements the IDynamicElement interface.
     */
    public void recordDynElem(String key, IDynamicElement value) {
        dynamics.put(key, value);
    }
    
    /**
     * Returns the analyser that parses template documents
     * (Note: You do not need to directly call this method to use JDynamiTe).
     *
     * @return The current analyser.
     * @see #setAnalyser(cb.jdynamite.analyser.ITemplateAnalyser)
     */
    public ITemplateAnalyser getAnalyser() {
        return analyser;
    }
    
    /**
     * Sets the analyser that parses template documents
     * (Note: You do not need to directly call this method to use JDynamiTe).
     *
     * @param templateAnalyser The new analyser.
     * @see #getAnalyser()
     */
    public void setAnalyser(ITemplateAnalyser templateAnalyser) {
        analyser = templateAnalyser;
    }
    
    /**
     * This is a "key" method you call to append elements to the current value of a Dynamic Element.
     * These elements are built from the template definition of this Dynamic Element and the current value
     * of variables (and possibly nested Dynamic Elements). See examples.<br>
     * See also <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     *
     * @param elementName The "Dynamic Element" identifier. This is the string defined
     * in the template document in the "BEGIN DYNAMIC" tag. For example:<pre>
     * &lt;-- BEGIN DYNAMIC : myList --&gt;
     * </pre>
     * Here "myList" identifies the Dynamic Element that begins.
     */
    public void parseDynElem(String elementName) {
        IDynamicElement dynElem = getDynElem(elementName);
        if (dynElem != null) {
            dynElem.parse(this);
        }
    }
    
    /**
     * Parses an XML Dynamic Element.<br>
     * It means that this XML Dynamic Element will be parsed using its XML input (URI or filename) and its XPath expression.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     * 
     * @param elementName "XML Dynamic Element" identifier.
     * @param recurse If true, this XML Dynamic Element will be recursively parsed (if it contains nested XML Elements).
     */
    public void parseXMLDynElem(String elementName, boolean recurse) {
        IDynamicElement dynElem = getDynElem(elementName);
        if (dynElem != null)
            dynElem.parseXML(this, recurse);
    }

    /**
     * Parses recursively all "first level" XML Dynamic Elements.<br>
     * It means that all XML Dynamic Elements defined in the template document will be parsed, using their XML input (URI or filename) and
     * their XPath expression.<br>
     * See <a href="#jdyn_elem">JDynamiTe elements</a> and <a href="#template_syntax">Template document syntax</a> sections.
     * 
     */
    public void parseXMLDynElem() {
        Iterable<IDynamicElement> dynElemValues = dynamics.values();
        for (IDynamicElement iDynElement : dynElemValues) {
            if (iDynElement.isXMLDynElement() && iDynElement.getFatherDynElem() == this) {
                iDynElement.parseXML(this, true);
            }
        }
    }

    public static void setVerbose(boolean isVerbose) {
        JDynamiTe.verbose = isVerbose;
        XMLUtil.setVerbose(JDynamiTe.verbose);
    }
    
    /**
     * Sets an arbitrary value to a Dynamic Element. For example, enables emptying a
     * Dynamic Element if you give an empty string as argument.
     *
     * @param elementName The Dynamic Element identifier.
     * @param value New value for The Dynamic Element
     */
    public void setDynElemValue(String elementName, String value) {
        IDynamicElement dynElem = getDynElem(elementName);
        if (dynElem != null) {
            dynElem.setValue(value);
        }
    }
    
    /**
     * Calls the parse method inherited from DefaultDynamicElement. As this object is the "top level"
     * dynamic element, this method enables obtaining the "final" value of this document.
     * You can use the "toString" method to get this value.
     *
     * @see #toString()
     */
    public void parse() {
        parse(this);
    }

    /**
     * Internal JDynamiTe method used to set error message, after parsing Dynamic Elements.
     * You do not have to use this method, which is internally called by Dynamic Elements.
     * Note that this method also sets the "INPUT_TEMPLATE_ERROR_TAG" special TAG.
     * @see #getLastError()
     * @see #INPUT_TEMPLATE_ERROR_TAG
     */
    public void setLastError(String lastError) {
        this.lastError = lastError;
        setVariable("__LAST_JDYN_ERROR__", (lastError == null) ? "" : lastError);
    }

    /**
     * Returns error message, possibly set after parsing Dynamic Elements.
     * Note that the "INPUT_TEMPLATE_ERROR_TAG" special TAG also contains this error message.
     * @return lastError Last error message, null if no error occurred.
     * @see #INPUT_TEMPLATE_ERROR_TAG
     */
    public String getLastError() {
        return lastError;
    }

    /**
     * Returns the value (after parsing) of this JDynamiTe document.
     * @return Value (after parsing) of this JDynamiTe document
     */
    public String toString() {
        return getValue(this);
    }
    
    /**
     * For debugging purposes: get the result (structure of the template document) of the analyser parsing.
     * @return Result (structure of the template document) of the analyser parsing.
     */
    public String getTemplateDefinition() {
        return getDefinition(0);
    }
    
    /**
     * Resets all Variables to empty string and all Dynamic Element values to null.
     * @see #resetAllVariables()
     * @see #resetAllDynElemValues()
     */
    public void resetAll() {
        resetAllVariables();
        resetAllDynElemValues();
    }
    
    /**
     * Resets all Variables values to empty String.
     */
    public void resetAllVariables() {
        Enumeration<String> keys = getVariableKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            setVariable(key, "");
        }
    }
    
    /**
     * Resets all Dynamic Element values to null.
     */
    public void resetAllDynElemValues() {
        Collection<IDynamicElement> dynElems = dynamics.values();
        for (IDynamicElement dynElem : dynElems) {
            dynElem.setValue(null);
        }
    }
    
    /**
     * Clears XML external Documents shared list.
     * These input documents are attached to XML Dynamic Elements.
     * JDynamiTe manages this global "cache" list of already loaded Documents, in order to optimize performances.
     * @see #setExternalXMLDocumentsCacheMaxSize(int)
     */
    public void resetExternalXMLDocuments() {
        XMLUtil.resetCache();
    }
    
    /**
     * Sets XML external Documents shared list maximum size.
     * JDynamiTe manages this global "cache" list of already loaded Documents, in order to optimize performances.
     * The default value of list maximum size is 10.
     * @param cacheMaxSize New value of list maximum size (must be > 0).
     * @see #resetExternalXMLDocuments()
     */
    public void setExternalXMLDocumentsCacheMaxSize(int cacheMaxSize) {
        XMLUtil.setCacheMaxSize(cacheMaxSize);
    }

}
